# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# # Batch ClassyFirer
# %% [markdown]
# Building on pyMolNetEnhancer (https://pypi.org/project/pyMolNetEnhancer/) this notebook allows to fetch classyfire for large structure tables.
# The main mnodification concern the ability to fetch classyfire results for inchi (or smiles) based input. 
# The paginated JSON results are fetched using the xxxxx function.
# %% [markdown]
# Load libraries required by pyMolNetEnhancer

# %%
from pybatchclassyfire import *
import pandas as pd
import os
import csv 
import time
import json
from pandas import json_normalize

# %% [markdown]
# Set option for display in pandas

# %%
#pd.reset_option('all')


# %%
pd.set_option('max_rows', 40)
pd.set_option('max_columns', 40)
#pd.set_option('display.max_colwidth', 3000)

# %% [markdown]
# ## Original data loading

# %%
input_table = pd.read_csv("/Users/pma/Dropbox/Research_UNIGE/git_repos/lockdownmassspecchallenge/createdData/siriusResults.tsv", sep = "\t")
input_table.info()

# %% [markdown]
# First we set the short InChIKey column name as a variable

# %%
shortik_colname = 'InChIkey2D'

# %% [markdown]
# Duplicates are droped at the sanitized inchi columns

# %%

input_table.drop_duplicates(shortik_colname, inplace = True)

# %% [markdown]
# Uncomment if a minimal input is needed for testing purposes

# %%
input_table = input_table.head(3000)

# %% [markdown]
# Fetch info on the loaded df

# %%
input_table.info()


# %%
input_table.head()

# %% [markdown]
# ## Retrieve ClassyFire classifications
# %% [markdown]
# This first step is done using inchikey and interrogation of the gnps classified structures

# %%
gnps_proxy = True


# %%
url = "http://classyfire.wishartlab.com"
proxy_url =  "https://gnps-classyfire.ucsd.edu"
chunk_size = 1000
sleep_interval = 12

# %% [markdown]
# Below is a slightly modified version of the original get_classification() which will take list object as input
# %% [markdown]
# The while loop below allows to run the get_classifications() function until the number of classified inchikey stablizes. Indeed, it has been observed that when running the get_classifications() multiple times the number of classified structures was different. Since the requests are stocked in the .sqlite file implemenmting the while loop allows to fetch the maximum of structures by iterations.
# The reason why the get_classifications() is not directly returning the maximal number of classified structures is unknown. It might be due to GET request errors which remain silent because of the parralelization of the function ?

# %%
all_inchi_keys = list(input_table[shortik_colname].drop_duplicates())

all_inchi_keys
resolved_ik_number_list = [0, 0]
ik_list = all_inchi_keys
#total_inchikey_number = len(ik_list)

#all_json = run_parallel_job(get_structure_class_entity, all_inchi_keys, parallelism_level = 8)

#type(all_json)
all_json = []

while True:

    start_time = time.time()

    print('%s inchikey to resolve' % len(ik_list))

    batch_json = []

    batch_json = run_parallel_job(
        get_structure_class_entity, ik_list, parallelism_level=8)


    if batch_json != [None, None] and batch_json != [None] and batch_json != []:

        all_json.extend(batch_json)

        open("batch_json.json", "w").write(json.dumps(batch_json))
        open("all_json.json", "w").write(json.dumps(all_json))

        cleanse('batch_json.json', 'batch_json.json')

        with open("batch_json.json") as tweetfile:
            jsondic = json.loads(tweetfile.read())

        df = json_normalize(jsondic)
        df = df.drop_duplicates('inchikey')
        # line below is to uncomment if you use full IK
        df.inchikey = df.inchikey.str.split("-", expand=True)[0]

        classified_ik_list = list(df.drop_duplicates(
            'inchikey').inchikey.str.replace(r'InChIKey=', ''))
        #nan removal
        classified_ik_list = [x for x in ik_list if str(x) != 'nan']

        print(classified_ik_list)
        print('classified_ik_list lenght at round n is %s' % len(set(classified_ik_list)))
        #nan removal
        classified_ik_list = [x for x in ik_list if str(x) != 'nan']
        

        print('iklist lenght at round n is %s' % len(set(ik_list)))
        ik_list = list(set(ik_list) - set(classified_ik_list))
        print('iklist lenght at round n+1 is %s' % len(set(ik_list)))
        print(ik_list)

        resolved_ik_number = len(df.drop_duplicates('inchikey').inchikey)
        resolved_ik_number_list.append(resolved_ik_number)
        print('%s resolved inchikeys' % resolved_ik_number)
        print("done in --- %s seconds ---" % (time.time() - start_time))
    else:
        print('Empty batch json')
        break

    #if resolved_ik_number_list[-1] < resolved_ik_number_list[-2] or resolved_ik_number_list[-1] == resolved_ik_number_list[-3]:
    if resolved_ik_number_list[-1] == resolved_ik_number_list[-3]:
        print('%s inchikeys could not be resolved' % len(ik_list))
        break

# # %%
# all_inchi_keys = list(input_table['inchikey_sanitized'].drop_duplicates())

# all_inchi_keys
# resolved_ik_number_list = [0, 0]
# ik_list = all_inchi_keys

# while True:
    
#     start_time = time.time()
    
#     print('%s inchikey to resolve' % len(ik_list) )
#     get_classifications_cf_mod(ik_list, 8)
    
#     cleanse('all_json.json', 'all_json.json')
    
#     with open("all_json.json") as tweetfile:
#         jsondic = json.loads(tweetfile.read())

#     df = json_normalize(jsondic)
#     df = df.drop_duplicates( 'inchikey' )
#     resolved_ik_number = len( df.drop_duplicates('inchikey').inchikey )
#     resolved_ik_number_list.append( resolved_ik_number )
#     print('%s resolved inchikeys' % resolved_ik_number )
#     print("done in --- %s seconds ---" % (time.time() - start_time))
    
#     if resolved_ik_number_list[-1] < resolved_ik_number_list[-2] or resolved_ik_number_list[-1] == resolved_ik_number_list[-3]:
#     #if resolved_ik_number_list[-1] == resolved_ik_number_list[-3]:
#         print('%s inchikeys could not be resolved' % len(ik_list))

#         break
# %% [markdown]
# We then use the cleanse function to directly remove unclassified structures from the json. Else the json file is not treated by the json_normalize() function.
# To remove null entries from json inputs and output cleaned file we define the cleanse() function. Slightly adapted from https://stackoverflow.com/a/50531943

# %%
cleanse('all_json.json', 'all_json_cleaned.json')

# %% [markdown]
# We now load this cleaned json file

# %%
with open("all_json_cleaned.json") as tweetfile:
        jsondic = json.loads(tweetfile.read())

# %% [markdown]
# And normalize the output as a dataframe

# %%
flattened_classified_json = json_normalize(jsondic)

# %% [markdown]
# And have a peak into this new df

# %%
flattened_classified_json.info()


# %%
flattened_classified_json.drop_duplicates('inchikey').info()

# %% [markdown]
# We now want to output the unclassified IK
# %% [markdown]
# So first we strip the InChI= prefix of the previously returned df and keep it as a list

# %% [markdown]
# optional (if shortIK are used)

# %%
flattened_classified_json.inchikey = flattened_classified_json.inchikey.str.split("-", expand=True)[0]

# %%
classified_ik_list = list(flattened_classified_json['inchikey'].str.replace(r'InChIKey=', ''))

# %% [markdown]
# We now make the difference between the inputed ik list and the classified ik list

# %%
len(set(all_inchi_keys))


# %%
len(set(classified_ik_list))


# %%
len(set(all_inchi_keys) - set(classified_ik_list))

# %% [markdown]
# ## Merging the CF output with the original DB

# %%
flattened_df = flattened_classified_json.drop_duplicates('inchikey')

# %% [markdown]
# optional (if shortIK are used)

# %%
flattened_df.inchikey = flattened_df.inchikey.str.split("-", expand=True)[0]
# %%
flattened_df['inchikey'] = flattened_df['inchikey'].str.replace(r'InChIKey=', '')


# %%
#df_merged = pd.merge(input_table, flattened_df, left_on='inchikey_sanitized', right_on='inchikey', how='left')
df_merged = pd.merge(input_table, flattened_df, left_on='shortik_sanitized', right_on='inchikey', how='left')


# %%
df_merged.info()


# %%
df_merged_unclassed = df_merged[df_merged['inchikey'].isnull()]


# %%
df_merged_unclassed.drop_duplicates('inchi_sanitized', inplace = True)


# %%
df_merged_unclassed.to_csv('test_datatable_unclassed.tsv', sep = '\t', encoding="utf-8")
df_merged_unclassed.inchi_sanitized.to_csv('test_datatable_unclassed_inchi.tsv', sep = '\t', encoding="utf-8", header = False)

# %% [markdown]
# ## Classyfing the unclassified by inchi
# %% [markdown]
# Below is a modified version of the tabular_query() function which basically just launches a structure_query for all inchi or smile at a given column

# %%
query_ids = batch_query('test_datatable_unclassed.tsv',
                   'inchi_sanitized', dialect='excel-tab')


# %%
query_ids = ['4011919']


# %% [markdown]
# The status can also be checked manually at the following adress (just change the query id)
# http://classyfire.wishartlab.com/queries/3879356.json?page=1

# %% [markdown]
# These settings of the request_cache allow to retry when 429 (or other) type of errors are returned by the classyfire server. Most of the time when too many intents are made. Since this seems to be a random behaviour, fixing a time.sleep is not safe enough.
## Here there is a problem to solve because it looks like the sqlite cache is leading to errors when the first request is delayed

# %%
open("test_datatable_result_inchi.json", "w").write(json.dumps(get_results_multientry_multipage_patient(query_ids, return_format="json")))


# %% [markdown]
# For problematic outputs (500 erroro response the json cannot be fetch.) We get the sdf instead
# Else skip to 


# %%
sdf_query = get_results(3997040, return_format="sdf", blocking=False)


# %% [markdown]
# In order to be loaded by RDKit the SDF output needs to pe curated
# Indeed space are requested between info blocks
# additionalyy .rstrip allows to remove an annoying last empty line 


sdf_query = sdf_query.replace('\n>', '\n\n>').rstrip()



# %% [markdown]
# The sdf is saved as sdf object

print(sdf_query,  file=open('sdf_query_%s.sdf' % query_ids , 'w'))

# %% [markdown]
# Now the sdf fields need to be parsed


from rdkit import Chem
from rdkit.Chem import PandasTools


my_sdf_file = 'sdf_query_[3997040].sdf'

my_sdf_file

sdf_frame = PandasTools.LoadSDF(my_sdf_file,
                            smilesName=None,
                            embedProps=True,
                            molColName=None,
                            includeFingerprints=False)


sdf_frame.to_csv('sdf_query_tabled.csv')

# %% [markdown]
# ## Outputs standardization 
# 
# Now we will standardize the json output of classifire get_entity() and the one of get_results_multipage_patient()
# %% [markdown]
# We will use the cleanse() function which as been previously defined to remove null entries from the json and normalize it.
# %% [markdown]
# ### For round 1
# %% [markdown]
# For the output of the get_results_multipage_patient() we first load the json as a dataframe and remove identities with an empty identifier value

# %%
with open("test_datatable_result_inchi.json") as tweetfile:
    jsondic_inchi = json.loads(tweetfile.read())

# %% [markdown]
# The json_normalize function is used to flatten the nested JSON structure.
# Beware here the meta = ['id'] field can sometimes return an error. Remove if you dont need it.

# %%
normalized_df_inchi = json_normalize(jsondic_inchi,
                              record_path = 'entities',
                               meta = ['label']
                              )


# %%
normalized_df_inchi.head()

# %% [markdown]
# And now we remove rows for wich no identifier is returned

# %%
normalized_df_inchi_nona = normalized_df_inchi[normalized_df_inchi['identifier'].notna()]

# %% [markdown]
# Finally the previous df is outputed ad a json file using the 'records' option

# %%
normalized_df_inchi_nona.to_json(r'result_json_all_cleaned.json', orient='records')

# %% [markdown]
# ## Merging both outputs

# %%
flattened_classified_json.inchikey

# %% [markdown]
# ## Comment here according to sdf or json formnated inchi queries
# %%
frames = [flattened_classified_json, normalized_df_inchi_nona]
#frames = [flattened_classified_json, sdf_frame]


result = pd.concat(frames)

result.info()

# %% [markdown]
# ### Here we can optionnaly format the output of a sdf formatted retrieved ClassyFire query
sdf_frame.info()


# %%

sdf_frame.rename(columns={'InChIKey': 'inchikey',
                      'SMILES': 'smiles',
                      'Kingdom': 'kingdom.name',
                      'Superclass': 'superclass.name',
                      'Class': 'class.name',
                      'Subclass': 'subclass.name',
                      'Intermediate Nodes': 'intermediate_nodes',
                      'Direct Parent': 'direct_parent.name',
                      'Alternative Parents': 'alternative_parents',
                      'Molecular Framework': 'molecular_framework',
                      'Substituents': 'substituents',
                      'Structure-based description': 'description',
                      'Ancestors': 'ancestors',
                      'External Descriptors': 'external_descriptor',
                      'ID': 'identifier'
                      }, inplace=True)


# %%
result.info()
# %%
result.drop_duplicates('inchikey', inplace=True)
# %%
normalized_df = result 

# %% [markdown]
# The resulting df can be exported as JSON file

# %%
result.to_json(r'results_full.json', orient='records')

# %% [markdown]
# This file can be reloaded for further expansion of nested fields (to pursue later on)

# %%
with open("results_full.json") as tweetfile:
    jsondic = json.loads(tweetfile.read())


# %%
normalized_df = json_normalize(jsondic,
                              record_path = 'intermediate_nodes',
                               meta = ['inchikey']
                              )


# %%
normalized_df = json_normalize(jsondic)


# %%
normalized_df.head()

# %% [markdown]
# For now we will select fields of interest
# %% [markdown]
# First we load the total json and we normailze it as a df

# %%
with open("results_full.json") as tweetfile:
    jsondic = json.loads(tweetfile.read())
    normalized_df = json_normalize(jsondic)

# %% [markdown]
# We display all columns

# %%
normalized_df.columns

# %% [markdown]
# We here make a list of unwanted coloumns name and we drop them

# %%
colstodrop = ['smiles', 'intermediate_nodes', 'alternative_parents',
              'molecular_framework', 'substituents', 'description',
              'external_descriptors', 'ancestors', 'predicted_chebi_terms',
              'predicted_lipidmaps_terms', 'classification_version',
              'kingdom.description', 'kingdom.chemont_id', 'kingdom.url',
              'superclass.description', 'superclass.chemont_id',
              'superclass.url',  'class.description', 'class.chemont_id',
              'class.url',  'subclass.description',
              'subclass.chemont_id', 'subclass.url',
              'direct_parent.description', 'direct_parent.chemont_id',
              'direct_parent.url', 'identifier']

normalized_df.drop(colstodrop, axis = 1, inplace = True)


# %%
normalized_df.info()


# %%
normalized_df.head()


# %%
normalized_df.drop_duplicates('inchikey').info()

# %% [markdown]
# Here we merge this classified df with the original df (after making sure bothe have been deduplicated and the InChIKey= string as been appended

# %%
input_table.drop_duplicates('inchi_sanitized', inplace = True)


# %%
normalized_df['inchikey'] = normalized_df['inchikey'].str.replace(r'InChIKey=', '')


# %%
df_final_classy = pd.merge(input_table, normalized_df, left_on='inchikey_sanitized', right_on='inchikey', how='left')


# %%
df_final_classy.drop_duplicates('inchikey_sanitized').info()


# %%
df_final_classy.to_csv('final_classy.tsv', sep = '\t')



# %%
